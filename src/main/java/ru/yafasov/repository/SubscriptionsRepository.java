package ru.yafasov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yafasov.model.Subscription;

import java.util.List;

public interface SubscriptionsRepository extends JpaRepository<Subscription, Integer> {
    List<Subscription> findAllByOwner_Id(Integer id);
    List<Subscription> findAllByOwnerIsNull();

    List<Subscription> findAllByTariffAndPeriod(String tariff, Integer period);
}
