
package ru.yafasov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.yafasov.model.FileInfo;

@Repository
public interface FilesInfoRepository extends JpaRepository<FileInfo, Long> {
    FileInfo findByStorageName(String storageName);
}

