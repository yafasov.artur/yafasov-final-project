package ru.yafasov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yafasov.model.User;

import java.util.Optional;


public interface UsersRepository extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String email);

}