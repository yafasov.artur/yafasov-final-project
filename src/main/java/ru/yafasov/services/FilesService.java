
package ru.yafasov.services;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

public interface FilesService {
    void saveFile(Integer ownerId, String description, MultipartFile multipartFile);

    void addFileToResponse(String fileName, HttpServletResponse response);
}
