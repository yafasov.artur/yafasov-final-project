package ru.yafasov.services;

import ru.yafasov.forms.SignUpForm;

public interface SignUpService {
    void signUpUser(SignUpForm form);
}
