package ru.yafasov.services;

import ru.yafasov.forms.UserForm;
import ru.yafasov.model.Subscription;
import ru.yafasov.model.User;

import java.util.List;

public interface UsersService {
    void addUser(UserForm form);
    List<User> getAllUsers();

    void deleteUser(Integer userId);

    User getUser(Integer userId);

    List<Subscription> getSubscriptionsByUser(Integer userId);

    List<Subscription> getSubscriptionsWithoutOwner();

    void addSubscriptionToUser(Integer userId, Integer subscriptionId);

    void update(Integer userId, UserForm userForm);
}