package ru.yafasov.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.yafasov.exceptions.UserNotFoundException;
import ru.yafasov.forms.UserForm;
import ru.yafasov.model.Subscription;
import ru.yafasov.model.User;
import ru.yafasov.repository.SubscriptionsRepository;
import ru.yafasov.repository.UsersRepository;

import java.util.List;
@RequiredArgsConstructor
@Component
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    private final SubscriptionsRepository subscriptionsRepository;

//    @Autowired
//    public UsersServiceImpl(UsersRepository usersRepository, SubscriptionsRepository subscriptionsRepository) {
//        this.usersRepository = usersRepository;
//        this.subscriptionsRepository = subscriptionsRepository;
//    }

    @Override
    public void addUser(UserForm form) {
        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .build();

        usersRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public void deleteUser(Integer userId) {
        usersRepository.deleteById(userId);
    }

    @Override
    public User getUser(Integer userId) {
        return usersRepository.findById(userId).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public List<Subscription> getSubscriptionsByUser(Integer userId) {
        return subscriptionsRepository.findAllByOwner_Id(userId);
    }

    @Override
    public List<Subscription> getSubscriptionsWithoutOwner() {
        return subscriptionsRepository.findAllByOwnerIsNull();
    }

    @Override
    public void addSubscriptionToUser(Integer userId, Integer subscriptionId) {
        User user = usersRepository.getById(userId);
        Subscription subscription = subscriptionsRepository.getById(subscriptionId);
        subscription.setOwner(user);
        subscriptionsRepository.save(subscription);
    }

    @Override
    public void update(Integer userId, UserForm userForm) {
        User user = usersRepository.getById(userId);
        user.setFirstName(userForm.getFirstName());
        user.setLastName(userForm.getLastName());
        usersRepository.save(user);
    }
}
