package ru.yafasov.model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.text.DecimalFormat;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity

public class Subscription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String tariff;

    private Integer period;

    private DecimalFormat price;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;
}


